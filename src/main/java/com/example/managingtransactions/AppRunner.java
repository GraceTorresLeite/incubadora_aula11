package com.example.managingtransactions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
class AppRunner implements CommandLineRunner {

	private final static Logger logger = LoggerFactory.getLogger(AppRunner.class);

	private final BookingService bookingService;

	public AppRunner(BookingService bookingService) {
		this.bookingService = bookingService;
	}

	@Override
	public void run(String... args) throws Exception {
		bookingService.book("Grace", "Lele", "Luiz");
		Assert.isTrue(bookingService.findAllBookings().size() == 3,
				"First booking should work with no problem");
		logger.info("Grace, Lele and Luiz have been booked");
		try {
			bookingService.book("Vanda", "Leticia");
		} catch (RuntimeException e) {
			logger.info("v--- The following exception is expect because 'Leticia' is too " +
					"big for the DB ---v");
			logger.error(e.getMessage());
		}

		for (String person : bookingService.findAllBookings()) {
			logger.info("So far, " + person + " is booked.");
		}
		logger.info("You shouldn't see Vanda or Leticia. Leticia violated DB constraints, " +
				"and Vanda was rolled back in the same TX");
		Assert.isTrue(bookingService.findAllBookings().size() == 3,
				"'Leticia' should have triggered a rollback");

		try {
			bookingService.book("Maria", null);
		} catch (RuntimeException e) {
			logger.info("v--- The following exception is expect because null is not " +
					"valid for the DB ---v");
			logger.error(e.getMessage());
		}

		for (String person : bookingService.findAllBookings()) {
			logger.info("So far, " + person + " is booked.");
		}
		logger.info("You shouldn't see Maria or null. null violated DB constraints, and " +
				"Maria was rolled back in the same TX");
		Assert.isTrue(bookingService.findAllBookings().size() == 3,
				"'null' should have triggered a rollback");
	}

}
